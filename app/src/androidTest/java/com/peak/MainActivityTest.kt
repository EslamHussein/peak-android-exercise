package com.peak

import androidx.test.rule.ActivityTestRule
import com.peak.shapes.MainActivityRobot
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class MainActivityTest {


    @get:Rule
    public val activityTest: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java, true, false)


    @Test
    fun test() {
        MainActivityRobot().lunch(activityTest)
            .clickOnCircleButton()
            .clickOnSquareButton()
            .clickOnTriangleButton()
            .clickOnUndo()
            .clickOnStatus()
    }
}