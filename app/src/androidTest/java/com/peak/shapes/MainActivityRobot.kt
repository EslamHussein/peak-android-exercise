package com.peak.shapes

import android.app.Activity
import android.content.Intent
import androidx.test.rule.ActivityTestRule
import com.peak.R

class MainActivityRobot : ScreenRobot<MainActivityRobot>() {

    fun <A : Activity> lunch(rule: ActivityTestRule<A>): MainActivityRobot {
        val intent = Intent()
        rule.launchActivity(intent)
        return this
    }

    fun clickOnCircleButton(): MainActivityRobot {
        return click(R.id.circleButton)
    }

    fun clickOnTriangleButton(): MainActivityRobot {
        return click(R.id.triangleButton)
    }

    fun clickOnSquareButton(): MainActivityRobot {
        return click(R.id.squareButton)
    }

    fun clickOnUndo(): MainActivityRobot {
        return click(R.id.undo_action)
    }

    fun clickOnStatus(): MainActivityRobot {
        return click(R.id.status_action)
    }
}