package com.peak.shapes

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matchers

abstract class ScreenRobot<T> {


    fun checkIsHidden(@IdRes vararg id: Int): T {
        id.forEach {
            onView(withId(it)).check(matches(Matchers.not(isDisplayed())))
        }
        return this as T
    }

    fun checkIsDisplayed(@IdRes vararg id: Int): T {
        id.forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }
        return this as T
    }

    fun checkViewHasText(viewId: Int, stringId: Int): T {

        onView(withId(viewId)).check(matches(ViewMatchers.withText(stringId)))

        return this as T
    }

    fun click(@IdRes viewId: Int): T {

        onView(withId(viewId)).perform(ViewActions.click())

        return this as T
    }
}


