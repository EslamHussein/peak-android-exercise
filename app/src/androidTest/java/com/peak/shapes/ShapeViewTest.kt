package com.peak.shapes

import android.content.Context
import android.widget.FrameLayout
import androidx.test.platform.app.InstrumentationRegistry
import com.peak.R
import com.peak.drawingboard.PositionGenerator
import com.peak.drawingboard.listener.OnShapeActionListener
import com.peak.flow.ShapesFlow
import com.peak.shapes.drawer.factory.DrawerFactory
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ShapeViewTest {

    private lateinit var context: Context


    private lateinit var positionGenerator: PositionGenerator
    private lateinit var params: FrameLayout.LayoutParams
    private lateinit var viewToTest: ShapeView


    @Mock
    private lateinit var onShapeActionListener: OnShapeActionListener


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        context = InstrumentationRegistry.getInstrumentation().targetContext
        positionGenerator = PositionGenerator(
            context.resources.getDimension(R.dimen.width_for_test).toInt(),
            context.resources.getDimension(R.dimen.height_for_test).toInt()
        )

        params = FrameLayout.LayoutParams(
            context.resources.getDimension(R.dimen.default_shape_width).toInt(),
            context.resources.getDimension(R.dimen.default_shape_height).toInt()
        )


        val point = positionGenerator.getRandomPortion(params.width, params.height)
        params.leftMargin = point.x
        params.topMargin = point.y

        viewToTest = ShapeView(context = context, attr = null).apply {
            id = 1
            drawer = DrawerFactory.getDrawer(ShapeType.SQUARE)
        }

        viewToTest.pathFlow = ShapesFlow.createFlow(ShapeType.SQUARE)
        viewToTest.layoutParams = params
        viewToTest.setOnShapeChangeListener(onShapeActionListener)

    }


    @Test
    fun testViewNotNull() {
        assertNotNull(viewToTest)

    }

    @Test
    fun testViewIsSquare() {
        assertEquals(viewToTest.getType(), ShapeType.SQUARE)
    }

    @Test
    fun testNextTransform() {

        viewToTest.callOnClick()

        assertEquals(viewToTest.getType(), ShapeType.CIRCLE)

        Mockito.verify(onShapeActionListener).onTransform(viewToTest)
    }

    @Test
    fun testUndoTransform() {

        viewToTest.undo()
        assertEquals(viewToTest.getType(), ShapeType.TRIANGLE)

    }

    @Test
    fun testLongPressToDelete() {

        viewToTest.onLongClick(viewToTest)
        Mockito.verify(onShapeActionListener).onDelete(viewToTest)

    }


}