package com.peak.status

import android.app.Activity
import android.content.Intent
import androidx.test.rule.ActivityTestRule
import com.peak.R
import com.peak.shapes.ScreenRobot
import java.util.ArrayList

class StatusActivityRobot : ScreenRobot<StatusActivityRobot>() {

    fun <A : Activity> lunch(rule: ActivityTestRule<A>, status: ArrayList<Status>?): StatusActivityRobot {
        val intent = Intent()
        intent.putParcelableArrayListExtra(STATUS_ARG, status)
        rule.launchActivity(intent)
        return this
    }

    fun emptyShapes(): StatusActivityRobot {
        checkIsDisplayed(R.id.errorTextView)
        return checkIsHidden(R.id.statusRecyclerView)
    }

    fun haveShapes(): StatusActivityRobot {
        checkIsHidden(R.id.errorTextView)

        return checkIsDisplayed(R.id.statusRecyclerView)

    }


}