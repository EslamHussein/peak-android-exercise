package com.peak.status

import androidx.test.rule.ActivityTestRule
import com.peak.MainActivity
import com.peak.shapes.ShapeType
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class StatusActivityTest {


    @get:Rule
    public val activityTest: ActivityTestRule<StatusActivity> =
        ActivityTestRule(StatusActivity::class.java, true, false)


    @Test
    fun testIsEmpty() {

        StatusActivityRobot().lunch(activityTest, null).emptyShapes()
    }

    @Test
    fun testHasShapes() {

        StatusActivityRobot().lunch(activityTest, arrayListOf(Status(ShapeType.TRIANGLE, 2))).haveShapes()
    }
}