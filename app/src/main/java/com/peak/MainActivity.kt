package com.peak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.peak.shapes.ShapeType
import com.peak.status.StatusActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        circleButton.setOnClickListener(this)
        triangleButton.setOnClickListener(this)
        squareButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.circleButton -> {
                drawingBoard.addShape(ShapeType.CIRCLE)
            }
            R.id.triangleButton -> {
                drawingBoard.addShape(ShapeType.TRIANGLE)
            }
            R.id.squareButton -> {
                drawingBoard.addShape(ShapeType.SQUARE)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.undo_action -> {
                drawingBoard.undo()
                true
            }


            R.id.status_action -> {

                StatusActivity.lunchActivity(this, ArrayList(drawingBoard.getStatus()))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
