package com.peak.drawingboard

import com.peak.shapes.ShapeView

class Action(val view: ShapeView, var operation: Operation) {
    override fun toString(): String {
        return "{${view.getType()} - $operation }"
    }
}
