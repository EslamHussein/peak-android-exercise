package com.peak.drawingboard

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.peak.status.Status
import com.peak.drawingboard.listener.OnShapeActionListener
import com.peak.shapes.*
import java.util.*
import com.peak.R
import com.peak.flow.ShapesFlow
import com.peak.shapes.drawer.factory.DrawerFactory


class DrawingBoardLayout(context: Context, attr: AttributeSet) : RelativeLayout(context, attr), OnShapeActionListener {


    private var idGenerator: Int = 0

    private val undoStack = Stack<Action>()

    private val positionGenerator by lazy {
        PositionGenerator(width, height)
    }


    fun addShape(shapeType: ShapeType) {


        val params = FrameLayout.LayoutParams(
            resources.getDimension(R.dimen.default_shape_width).toInt(),
            resources.getDimension(R.dimen.default_shape_width).toInt()
        )

        val point = positionGenerator.getRandomPortion(params.width, params.height)
        params.leftMargin = point.x
        params.topMargin = point.y

        val viewToAdd = ShapeView(context = context, attr = null).apply {
            id = ++idGenerator
            drawer = DrawerFactory.getDrawer(shapeType)
        }

        viewToAdd.pathFlow = ShapesFlow.createFlow(shapeType)
        viewToAdd.layoutParams = params
        addShape(viewToAdd)


    }

    private fun addShape(shape: ShapeView, fromDelete: Boolean = false) {

        shape.setOnShapeChangeListener(this)

        this.addView(shape, shape.layoutParams)
        if (!fromDelete)
            undoStack.push(Action(shape, Operation.ADD))

    }

    fun undo() {

        if (undoStack.isEmpty())
            return
        val action = undoStack.pop()

        when (action.operation) {
            Operation.ADD -> {
                this.removeView(action.view)
            }
            Operation.TRANSFORM -> {
                action.view.undo()
            }
            Operation.DELETE -> {
                addShape(action.view, true)
            }
        }

    }


    override fun onDelete(view: ShapeView) {
        this.removeView(view)
        undoStack.push(Action(view, Operation.DELETE))
    }

    override fun onTransform(view: ShapeView) {
        undoStack.push(Action(view, Operation.TRANSFORM))

    }

    fun getStatus() = undoStack.groupBy {
        it.view.id
    }.mapValues {
        it.value.last()
    }.filterValues {
        it.operation != Operation.DELETE
    }.map {
        it.value
    }.groupBy {
        it.view.getType()
    }.mapValues {
        it.value.size
    }.map {
        Status(it.key!!, it.value)
    }


}