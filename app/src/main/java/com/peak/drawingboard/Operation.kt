package com.peak.drawingboard

enum class Operation {
    ADD, TRANSFORM, DELETE
}
