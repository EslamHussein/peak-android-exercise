package com.peak.drawingboard

import android.graphics.Point

class PositionGenerator(private val width: Int, private val height: Int) {
    fun getRandomPortion(shapeWidth: Int, shapeHeight: Int): Point {
        val x = (0..(width - shapeWidth)).shuffled().first()
        val y = (0..(height - shapeHeight)).shuffled().first()
        return Point(x, y)
    }
}