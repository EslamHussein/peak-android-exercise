package com.peak.drawingboard.listener

import com.peak.shapes.ShapeView

interface OnShapeActionListener {

    fun onTransform(view: ShapeView)
    fun onDelete(view: ShapeView)


}



