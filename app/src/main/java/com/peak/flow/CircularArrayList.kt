package com.peak.flow

class CircularArrayList<T> {
    private val TAG = "CArrayList"

    private var currentIndex = 0


    private val mutableList: MutableList<T> = mutableListOf()

    internal fun insertAtEnd(item: T, isStart: Boolean) {
        mutableList.add(item)
        if (isStart)
            currentIndex = mutableList.size - 1
    }

    fun goNext() {

        if (currentIndex == mutableList.size - 1)
            currentIndex = 0
        else {
            ++currentIndex
        }
    }

    fun goPrev() {
        if (currentIndex == 0)
            currentIndex = mutableList.size - 1
        else {
            --currentIndex
        }

    }

    fun getCurrent(): T? {
        return if (mutableList.isEmpty()) null else mutableList[currentIndex]


    }


}