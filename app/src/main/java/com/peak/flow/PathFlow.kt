package com.peak.flow


class PathFlow<T> {
    private val list = CircularArrayList<T>()

    fun addFlow(flow: T, isStart: Boolean): PathFlow<T> {
        list.insertAtEnd(flow,isStart)
        return this
    }

    fun goNext(transform: (value: T?) -> Unit) {
        list.goNext()
        transform(list.getCurrent())
    }

    fun goPrev(transform: (value: T?) -> Unit) {
        list.goPrev()
        transform(list.getCurrent())
    }


}