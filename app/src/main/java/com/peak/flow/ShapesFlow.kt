package com.peak.flow

import com.peak.shapes.ShapeType
import com.peak.shapes.drawer.Drawer
import com.peak.shapes.drawer.factory.DrawerFactory

class ShapesFlow {

    companion object {
        fun createFlow(startPoint: ShapeType): PathFlow<Drawer> {
            val squareFlow = DrawerFactory.getDrawer(ShapeType.SQUARE)
            val circleFlow = DrawerFactory.getDrawer(ShapeType.CIRCLE)
            val triangleFlow = DrawerFactory.getDrawer(ShapeType.TRIANGLE)

            return PathFlow<Drawer>()
                .addFlow(squareFlow, startPoint == ShapeType.SQUARE)
                .addFlow(circleFlow, startPoint == ShapeType.CIRCLE)
                .addFlow(triangleFlow, startPoint == ShapeType.TRIANGLE)
        }
    }


}