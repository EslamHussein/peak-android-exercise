package com.peak.shapes

enum class ShapeType {
    CIRCLE, TRIANGLE, SQUARE
}