package com.peak.shapes

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.peak.R
import com.peak.drawingboard.listener.OnShapeActionListener
import com.peak.flow.PathFlow
import com.peak.shapes.drawer.Drawer
import java.util.*


class ShapeView(context: Context, attr: AttributeSet?) : View(context, attr), View.OnClickListener,
    View.OnLongClickListener {


    private var onShapeChangeListener: OnShapeActionListener? = null

    private var mPaint = Paint()

    var drawer: Drawer? = null

    var pathFlow: PathFlow<Drawer>? = null


    init {
        mPaint.style = Paint.Style.FILL
        val rnd = Random()
        mPaint.color = Color.argb(
            255, rnd.nextInt(256),
            rnd.nextInt(256), rnd.nextInt(256)
        )
        mPaint.isAntiAlias = true

        this.setOnClickListener(this)
        this.setOnLongClickListener(this)

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val mPadding = Padding(paddingLeft, paddingRight, paddingTop, paddingBottom)
        drawer?.draw(mCanvas = canvas, mPoint = mPaint, width = width, height = height, padding = mPadding)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val desiredWidth = resources.getDimension(R.dimen.default_shape_width).toInt()
        val desiredHeight = resources.getDimension(R.dimen.default_shape_height).toInt()


        val widthX = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> Math.min(desiredWidth, widthSize)
            MeasureSpec.UNSPECIFIED -> desiredWidth
            else -> {
                desiredWidth
            }
        }

        val heightY = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> Math.min(desiredHeight, heightSize)
            MeasureSpec.UNSPECIFIED -> desiredHeight
            else -> desiredHeight
        }
        setMeasuredDimension(widthX, heightY);


    }

    override fun onClick(v: View?) {
        pathFlow?.goNext {
            drawer = it
        }
        invalidate()
        onShapeChangeListener?.onTransform(this)

    }


    fun undo() {

        pathFlow?.goPrev {
            drawer = it
        }
        invalidate()

    }

    fun setOnShapeChangeListener(onShapeChangeListener: OnShapeActionListener) {
        this.onShapeChangeListener = onShapeChangeListener
    }

    override fun onLongClick(v: View?): Boolean {

        onShapeChangeListener?.onDelete(this)
        return true
    }

    fun getType() = drawer?.shapeType


}