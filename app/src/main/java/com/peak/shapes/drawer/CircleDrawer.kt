package com.peak.shapes.drawer

import android.graphics.Canvas
import android.graphics.Paint
import com.peak.shapes.Padding
import com.peak.shapes.ShapeType

class CircleDrawer : Drawer {

    override val shapeType: ShapeType
        get() = ShapeType.CIRCLE

    override fun draw(mCanvas: Canvas?, width: Int, height: Int, padding: Padding, mPoint: Paint): Canvas? {
        val usableWidth = width - (padding.left +padding.right)
        val usableHeight = height - (padding.top + padding.bottom)

        val radius = Math.min(usableWidth, usableHeight) / 2
        val cx = padding.left + usableWidth / 2
        val cy = padding.top + usableHeight / 2


        mCanvas?.drawCircle(cx.toFloat(), cy.toFloat(), radius.toFloat(), mPoint)
        return mCanvas

    }

    override fun toString(): String {
        return shapeType.toString()
    }


}