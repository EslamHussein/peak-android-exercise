package com.peak.shapes.drawer

import android.graphics.Canvas
import android.graphics.Paint
import com.peak.shapes.Padding
import com.peak.shapes.ShapeType

interface Drawer {

    val shapeType: ShapeType

    fun draw(mCanvas: Canvas?, width: Int, height: Int, padding: Padding, mPoint: Paint): Canvas?

}