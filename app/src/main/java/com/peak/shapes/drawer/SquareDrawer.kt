package com.peak.shapes.drawer

import android.graphics.Canvas
import android.graphics.Paint
import com.peak.shapes.Padding
import com.peak.shapes.ShapeType


class SquareDrawer : Drawer {
    override fun draw(mCanvas: Canvas?, width: Int, height: Int, padding: Padding, mPoint: Paint): Canvas? {
        val usableWidth = width - (padding.left +padding.right)
        val usableHeight = height - (padding.top + padding.bottom)


        mCanvas?.drawRect(0.toFloat(), 0.toFloat(), usableWidth.toFloat(), usableHeight.toFloat(), mPoint)
        return mCanvas
    }

    override val shapeType: ShapeType
        get() = ShapeType.SQUARE


    override fun toString(): String {
        return shapeType.toString()
    }
}

