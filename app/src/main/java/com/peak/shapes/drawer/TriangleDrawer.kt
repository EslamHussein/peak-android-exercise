package com.peak.shapes.drawer

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import com.peak.shapes.Padding
import com.peak.shapes.ShapeType

class TriangleDrawer : Drawer {
    override val shapeType: ShapeType
        get() = ShapeType.TRIANGLE

    override fun draw(mCanvas: Canvas?, width: Int, height: Int, padding: Padding, mPoint: Paint): Canvas? {

        val usableWidth = width - (padding.left +padding.right)
        val usableHeight = height - (padding.top + padding.bottom)

        val halfWidth = (usableWidth / 2)

        val path = Path()
        path.moveTo(halfWidth.toFloat(), 0F) // Top
        path.lineTo(0F, usableHeight.toFloat()) // Bottom left
        path.lineTo(usableWidth.toFloat(), usableHeight.toFloat()) // Bottom right
        path.lineTo(halfWidth.toFloat(), 0F) // Back to Top
        path.close()
        mCanvas?.drawPath(path, mPoint)

        return mCanvas

    }

}