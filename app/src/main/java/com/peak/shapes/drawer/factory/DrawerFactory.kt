package com.peak.shapes.drawer.factory

import com.peak.shapes.ShapeType
import com.peak.shapes.drawer.CircleDrawer
import com.peak.shapes.drawer.Drawer
import com.peak.shapes.drawer.SquareDrawer
import com.peak.shapes.drawer.TriangleDrawer

class DrawerFactory {

    companion object {
        fun getDrawer(shapeType: ShapeType): Drawer {
            return when (shapeType) {
                ShapeType.SQUARE -> SquareDrawer()
                ShapeType.CIRCLE -> CircleDrawer()
                ShapeType.TRIANGLE -> TriangleDrawer()
            }
        }
    }

}