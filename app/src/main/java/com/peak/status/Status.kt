package com.peak.status

import android.os.Parcelable
import com.peak.shapes.ShapeType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Status(val shapeType: ShapeType, val count: Int) :Parcelable