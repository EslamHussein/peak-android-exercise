package com.peak.status

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.peak.R
import kotlinx.android.synthetic.main.activity_status.*
import java.util.ArrayList


const val STATUS_ARG = "status_arg"

class StatusActivity : AppCompatActivity() {

    private lateinit var statusAdapter: StatusAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);


        val status = intent.getParcelableArrayListExtra<Status>(STATUS_ARG)


        if (status == null || status.isEmpty()) {
            errorTextView.visibility = View.VISIBLE

        } else {

            linearLayoutManager = LinearLayoutManager(this@StatusActivity)
            statusAdapter = StatusAdapter(status)

            statusRecyclerView.apply {
                layoutManager = linearLayoutManager
                setHasFixedSize(true)
                adapter = statusAdapter


            }
            statusRecyclerView.visibility = View.VISIBLE
        }


    }


    companion object {

        fun lunchActivity(context: Context, status: ArrayList<Status>) {

            val intent = Intent(context, StatusActivity::class.java)
            intent.putParcelableArrayListExtra(STATUS_ARG, status)
            context.startActivity(intent)
        }
    }
}
