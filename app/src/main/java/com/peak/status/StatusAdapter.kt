package com.peak.status

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.peak.R
import com.peak.shapes.drawer.factory.DrawerFactory
import kotlinx.android.synthetic.main.status_item_view.view.*


class StatusAdapter(private val data: ArrayList<Status>) : RecyclerView.Adapter<StatusAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.status_item_view, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = data[position]
        holder.shapeNameTextView.text = item.shapeType.name
        holder.shapeCountTextView.text = item.count.toString()


        holder.shapeView.apply {
            drawer = DrawerFactory.getDrawer(item.shapeType)
        }

    }

    override fun getItemCount() = data.size


    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var shapeView = view.shapeView

        var shapeNameTextView = view.shapeNameTextView

        var shapeCountTextView = view.shapeCountTextView

    }
}
