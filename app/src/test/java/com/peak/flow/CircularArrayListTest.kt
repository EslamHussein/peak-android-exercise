package com.peak.flow

import org.junit.Assert.*
import org.junit.Test

class CircularArrayListTest {


    @Test
    fun testEmptyArray() {
        val cArrayList = CircularArrayList<String>()
        assertNull(cArrayList.getCurrent())
    }


    @Test
    fun testCurrentNotNull() {
        val cArrayList = CircularArrayList<String>()
        cArrayList.insertAtEnd("First", true)
        cArrayList.insertAtEnd("Second", false)
        cArrayList.insertAtEnd("Third", false)

        assertNotNull(cArrayList.getCurrent())
    }


    @Test
    fun testGoNext() {
        val cArrayList = CircularArrayList<String>()
        cArrayList.insertAtEnd("First", true)
        cArrayList.insertAtEnd("Second", false)
        cArrayList.insertAtEnd("Third", false)
        cArrayList.goNext()
        assertEquals(cArrayList.getCurrent(), "Second")
    }

    @Test
    fun testGoPrev() {
        val cArrayList = CircularArrayList<String>()
        cArrayList.insertAtEnd("First", true)
        cArrayList.insertAtEnd("Second", false)
        cArrayList.insertAtEnd("Third", false)
        cArrayList.goPrev()
        assertEquals(cArrayList.getCurrent(), "Third")
    }

}