package com.peak.flow

import org.junit.Assert.*
import org.junit.Test

class PathFlowTest {


    @Test
    fun testEmptyFlow() {

        val flow = PathFlow<String>()

        flow.goNext {
            assertNull(it)
        }
    }


    @Test
    fun testGoNextFlow() {

        val flow = PathFlow<String>()

        flow.addFlow("First", true)
        flow.addFlow("Second", false)
        flow.addFlow("Third", false)
        flow.goNext {
            assertEquals(it, "Second")
        }
    }

    @Test
    fun testGoPrevFlow() {

        val flow = PathFlow<String>()

        flow.addFlow("First", true)
        flow.addFlow("Second", false)
        flow.addFlow("Third", false)
        flow.goPrev {
            assertEquals(it, "Third")
        }
    }
}