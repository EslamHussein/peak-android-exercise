package com.peak.flow

import com.peak.shapes.ShapeType
import org.junit.Assert.*
import org.junit.Test

class ShapesFlowTest {


    @Test
    fun testFlowNotNull() {
        val flow = ShapesFlow.createFlow(ShapeType.CIRCLE)
        assertNotNull(flow)
    }
}