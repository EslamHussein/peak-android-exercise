package com.peak.shapes.drawer.factory

import com.peak.shapes.ShapeType
import org.junit.Assert.*
import org.junit.Test

class DrawerFactoryTest {


    @Test
    fun testCircleDrawer() {
        val circleDrawer = DrawerFactory.getDrawer(ShapeType.CIRCLE)
        assertNotNull(circleDrawer)
        assertEquals(circleDrawer.shapeType, ShapeType.CIRCLE)
    }


    @Test
    fun testTriangleDrawer() {
        val triangleDrawer = DrawerFactory.getDrawer(ShapeType.TRIANGLE)
        assertNotNull(triangleDrawer)
        assertEquals(triangleDrawer.shapeType, ShapeType.TRIANGLE)
    }
    fun testSquareDrawer() {
        val squareDrawer = DrawerFactory.getDrawer(ShapeType.SQUARE)
        assertNotNull(squareDrawer)
        assertEquals(squareDrawer.shapeType, ShapeType.SQUARE)
    }
}